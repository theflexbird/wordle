import express, { Application} from "express"
import router from "./route";
import db from "./db";


const app: Application = express();

app.set('db', db);

app.use(express.json());
app.use('/api', router);

export default app;
