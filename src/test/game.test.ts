import request from "supertest";
import app from "../server";
import db from "../db";

describe("POST game", () => {
  test("return id for created game", async () => {
    const { body } = await request(app).post("/api/game");

    expect(body.id).toMatch(
      /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/
    );
  });
});

describe("POST game/guess", () => {
  test("return error when game does not exist", async () => {
    const result = await request(app)
      .post("/api/game/guess")
      .send({ id: "some-random-id", word: "hello" });
    expect(result.status).toEqual(404);
    expect(result.body).toEqual({ error: "Game not found" });
  });

  test("return error when payload is incorrect", async () => {
    const result = await request(app)
      .post("/api/game/guess")
      .send({ id: "some-random-id" });
    expect(result.status).toEqual(422);
    expect(result.body).toEqual({ error: '"word" is required' });
  });

  test("should return information about win if words are the same", async () => {
    const { body } = await request(app).post("/api/game");

    // test database insertion (I use it like that to save time)
    const collection = db.getCollection("games");
    const game = collection.findOne({ id: body.id });
    game.word = "hello";
    collection.update(game);
    // end test database insertion

    const result = await request(app)
      .post("/api/game/guess")
      .send({ id: body.id, word: "hello" });

    expect(result.status).toEqual(200);
    expect(result.body).toEqual({
      correctLetters: ["h", "e", "l", "o"],
      correctPositions: ["h", "e", "l", "l", "o"],
      correctLettersNumber: 4,
      correctPositionsNumber: 5,
      win: true,
    });
  });

  test("should return information about correct letters and correct positions", async () => {
    const { body } = await request(app).post("/api/game");

    // test database insertion (I use it like that to save time)
    const collection = db.getCollection("games");
    const game = collection.findOne({ id: body.id });
    game.word = "melon";
    collection.update(game);
    // end test database insertion

    const result = await request(app)
      .post("/api/game/guess")
      .send({ id: body.id, word: "hello" });

    expect(result.status).toEqual(200);
    expect(result.body).toEqual({
      correctLetters: ["e", "l", "o"],
      correctPositions: ["e", "l"],
      correctLettersNumber: 3,
      correctPositionsNumber: 2,
      win: false,
    });
  });
});
