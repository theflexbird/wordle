import { Request, Response, NextFunction } from "express";

const middleware = (schema: any) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const { error } = schema.validate(req.body);
    const valid = error == null;

    if (valid) {
      next();
    } else {
      const { details } = error;
      const message = details.map((i: any) => i.message).join(",");

      res.status(422).json({ error: message });
    }
  };
};

export default middleware;
