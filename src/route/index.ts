import express from "express"
import { handler as gameRoot } from "./game"
import { handler as gameGuess, schema as guessSchema } from "./game/guess"
import middleware from "../middleware/payloadValidator"
const router = express.Router()

router.post('/game', gameRoot);
router.post('/game/guess',  middleware(guessSchema), gameGuess);

export default router;
