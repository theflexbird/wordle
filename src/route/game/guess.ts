import { Request, Response } from "express";
import Joi from "joi";
import Game from "../../model/game";
import GameGuessDTO from "../../dataType/guess.interface";

const schema = Joi.object({
    id: Joi.string().required(),
    word: Joi.string().required()
})

const handler = (req: Request, res: Response) => {
  const db = req.app.get("db");
  const game = new Game(db);
  const guess: GameGuessDTO = req.body;
  try {
    const result = game.validateWord(guess.id, guess.word);
    res.json({ ...result });
  } catch (e: any) {
    res.status(404).json({ error: e.message });
  }
};

export { handler, schema };
