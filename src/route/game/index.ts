import { Request, Response } from "express";
import Game from "../../model/game";

const handler = (req: Request, res: Response) => {
    const db = req.app.get('db');
    const game = new Game(db);
    const id = game.createGame();
  // call model to ask for data
  res.json({ id })
}

export { handler } ;
