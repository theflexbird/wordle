import lokijs from 'lokijs';

const db = new lokijs('sample.db');

db.addCollection('games');

export default db;
