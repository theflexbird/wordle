import { v4 as uuidv4 } from "uuid";
import GameResult from "../../dataType/gameResults.interface";

class Game {
  db: any;
  collection: any;

  constructor(db: any) {
    this.db = db;
    this.collection = db.getCollection("games");
  }

  createGame(): string {
    const id = uuidv4();
    const word = this.generateWord();
    this.collection.insert({ id, word });
    return id;
  }

  generateWord(): string {
    // list of example words with 5 letters
    const words = ["hello", "world", "apple", "grape", "melon", "water"];
    const index = Math.floor(Math.random() * words.length);
    return words[index];
  }

  validateWord(id: string, word: string): GameResult {
    const game = this.collection.findOne({ id });
    let win = false;
    if (!game) {
      throw new Error("Game not found");
    }

    const correctLetters = word
      .split("")
      .filter((letter, index, self) => self.indexOf(letter) == index)
      .filter((letter) => (game.word.includes(letter) ? letter : false));

    const correctPositions = word
      .split("")
      .filter((letter, index) => letter === game.word[index]);

    if (correctPositions.length === 5) {
      win = true;
    }

    return {
      correctLetters,
      correctPositions,
      correctLettersNumber: correctLetters.length,
      correctPositionsNumber: correctPositions.length,
      win,
    };
  }
}

export default Game;
