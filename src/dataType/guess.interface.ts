export default interface GameGuessDTO {
    id: string;
    word: string;
}
