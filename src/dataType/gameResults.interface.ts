export default interface GameResult {
    correctLetters: Array<string>;
    correctPositions: Array<string>;
    correctLettersNumber: number;
    correctPositionsNumber: number;
    win: boolean;
}
