### How to

I hope it will be super simple(no config, no magic) for you to check and run the app/test.

## Installation

After pulling the repo run `npm i` and that is it.

## Run application in dev mode

In order to run app use command `npm run dev`.

## Run tests

In order to run test use command `npm test`.
